import logging
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from keras import backend as K
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from scipy import stats
from sklearn import metrics

import data
import train
from evaluate import residual_score
from vailtools import losses
from vailtools import networks
from vailtools.callbacks import CyclicLRScheduler
from vailtools.data.splits import split_data


def arg_parser():
    parser = train.arg_parser()
    parser.description = 'Computes confidence intervals over neural network ' \
                         'performance statistics in order to identify the ' \
                         'impact of random initialization'
    parser.add_argument(
        '-R',
        '--repeats',
        default=30,
        type=int,
        help='Number of models to train and evaluate before calculating the confidence interval.',
    )
    parser.add_argument(
        '-p',
        '--percentile',
        default=0.95,
        type=float,
        help='Width of the computed confidence interval.',
    )
    return parser


def regression_score_conf_intervals(
        activation='selu',
        batch_size=8,
        cycles=5,
        depth=4,
        epochs=200,
        filters=16,
        final_activation='linear',
        h_flip=False,
        h_shift_range=0.,
        loss='mse',
        lr0=0.2,
        mode='regress_co',
        model_factory=networks.res_u_net,
        noise_std=0.003,
        percentile=0.95,
        repeats=60,
        rot_range=0,
        v_flip=False,
        verbose=False,
        w_shift_range=0.,
        zoom_range=0.,
        **kwargs,
):
    """
    Trains several Keras models created using model_factory and the provided training
    configuration.

    Targets regression problems, evaluating models using a score constructed from
    distribution statistics over the residuals.
    Calculates and returns a confidence interval for the mean of the test scores
    of the models.

    Args:
        activation: str, keras function, or tensorflow function,
        batch_size: int,
        cycles: int,
        depth: int,
        epochs: int,
        filters: int,
        final_activation: str, keras function, or tensorflow function,
        h_flip: bool,
        h_shift_range: float,
        loss: str, keras function, or tensorflow function,
        lr0: float,
        mode: str,
        model_factory: Callable[[**kwargs], keras.models.Model],
        noise_std: float,
        percentile: float,
        rot_range: int,
        repeats: int,
        v_flip: bool,
        verbose: bool,
        w_shift_range: float,
        zoom_range: float or tuple(float, float),

    Returns: float,
        Test error of the trained model.
    """
    # Prepare data and training set
    x, y = getattr(data, f'load_{mode.split("_")[-1]}_data')()
    train, val, test = split_data(
        x,
        y,
        save=True,
        save_path=f'../models/{model_factory.__name__}',
        save_name=f'{mode}_score_experiment_splits',
        process_func=getattr(data, f'process_{mode}_data')
    )
    batches_per_epoch = int(np.ceil(len(train.x) / batch_size))

    # Construct data generators
    x_aug = ImageDataGenerator(
        rotation_range=rot_range,
        width_shift_range=w_shift_range,
        height_shift_range=h_shift_range,
        horizontal_flip=h_flip,
        vertical_flip=v_flip,
        zoom_range=zoom_range,
        fill_mode='constant',
        cval=0,
    )
    y_aug = ImageDataGenerator(
        rotation_range=rot_range,
        width_shift_range=w_shift_range,
        height_shift_range=h_shift_range,
        horizontal_flip=h_flip,
        vertical_flip=v_flip,
        zoom_range=zoom_range,
        fill_mode='constant',
        cval=0,
    )

    train_gen = data.gen_batches(
        *train,
        batch_size=batch_size,
        x_aug=x_aug,
        y_aug=y_aug
    )
    test_gen = ImageDataGenerator()

    # Train and evaluate models
    residual_scores = []
    for i in range(repeats):
        # Construct training callbacks
        checkpoint = ModelCheckpoint(
            filepath=f'../models/{model_factory.__name__}/{mode}_scores_model.h5',
            save_best_only=True)
        logger = CSVLogger(f'../models/{model_factory.__name__}/{pd.Timestamp.now().date()}_{mode}_errors.csv')
        lr_schduler = CyclicLRScheduler(
            lr0=lr0,
            total_steps=epochs * batches_per_epoch,
            cycles=cycles
        )

        model = model_factory(
            activation=activation,
            depth=depth,
            filters=filters,
            final_activation=final_activation,
            input_shape=train.x.shape[1:],
            loss=loss,
            noise_std=noise_std,
        )
        if verbose and not i:
            model.summary(print_fn=logging.info)

        model.fit_generator(
            train_gen,
            steps_per_epoch=batches_per_epoch,
            epochs=epochs,
            validation_data=test_gen.flow(*val, batch_size=batch_size),
            callbacks=[checkpoint, logger, lr_schduler],
            verbose=verbose,
        )
        # Load best checkpoint
        model = load_model(
            f'../models/{model_factory.__name__}/{mode}_scores_model.h5',
            custom_objects={'iou_loss': losses.iou_loss}
        )

        # Evaluate performance on the test set
        test_pred = model.predict_generator(
            test_gen.flow(test.x, batch_size=batch_size, shuffle=False),
            verbose=verbose,
        )
        residuals = test_pred - test.y
        residual_scores.append(residual_score(residuals, components=True))

        logging.info(f'Residual score {i + 1}: {residual_scores[-1].sum()}')

        # Clean up tensorflow resources before continuing
        K.clear_session()

    scores_df = pd.DataFrame(
        np.stack(residual_scores),
        columns=['mean', 'std', 'skew'],
    )
    scores_df['score'] = -scores_df.abs().sum(axis=1)
    scores_df.to_csv(
        f'../models/{model_factory.__name__}/{mode}_scores.csv'
    )

    # Calculate score distribution statistics
    col2mean, col2sem, col2interval = dict(), dict(), dict()
    for col in scores_df.columns:
        vals = scores_df[col]
        mean = np.mean(vals)
        sem = stats.sem(vals)
        interval = stats.t.interval(
            percentile,
            len(vals) - 1,
            loc=mean,
            scale=sem,
        )
        col2mean[col] = mean
        col2sem[col] = sem
        col2interval[col] = interval
    logging.info(f'Score means: {col2mean}')
    logging.info(f'Score standard errors: {col2sem}')
    logging.info(f'Score mean {percentile * 100:0.2f}% confidence intervals: {col2interval}')
    return col2interval['score']


def roc_auc_conf_intervals(
        activation='selu',
        batch_size=8,
        cycles=5,
        depth=4,
        epochs=200,
        filters=16,
        final_activation='sigmoid',
        h_flip=False,
        h_shift_range=0.,
        loss='iou_loss',
        lr0=0.2,
        mode='classify_co',
        model_factory=networks.res_u_net,
        noise_std=0.,
        percentile=0.95,
        repeats=30,
        rot_range=0,
        v_flip=False,
        verbose=False,
        w_shift_range=0.,
        zoom_range=0.,
        **kwargs,
):
    """
    Trains several Keras models created using model_factory and the provided training
    configuration.

    Targets classification/segmentation problems, evaluating models using the
    Receiver Operating Characteristic Area Under Curve (ROC AUC) statistic.
    Calculates and returns a confidence interval for the mean of the test scores
    of the models.

    Args:
        activation: str, keras function, or tensorflow function,
        batch_size: int,
        cycles: int,
        depth: int,
        epochs: int,
        filters: int,
        final_activation: str, keras function, or tensorflow function,
        h_flip: bool,
        h_shift_range: float,
        loss: str, keras function, or tensorflow function,
        lr0: float,
        mode: str,
        model_factory: Callable[[**kwargs], keras.models.Model],
        noise_std: float,
        percentile: float,
        rot_range: int,
        repeats: int,
        v_flip: bool,
        verbose: bool,
        w_shift_range: float,
        zoom_range: float or tuple(float, float),

    Returns: float,
        Test error of the trained model.
    """
    # Prepare data and training set
    x, y = getattr(data, f'load_{mode.split("_")[-1]}_data')()
    train, val, test = split_data(
        x,
        y,
        save=True,
        save_path=f'../models/{model_factory.__name__}',
        save_name=f'{mode}_score_experiment_splits',
        process_func=getattr(data, f'process_{mode}_data')
    )
    batches_per_epoch = int(np.ceil(len(train.x) / batch_size))

    # Construct data generators
    x_aug = ImageDataGenerator(
        rotation_range=rot_range,
        width_shift_range=w_shift_range,
        height_shift_range=h_shift_range,
        horizontal_flip=h_flip,
        vertical_flip=v_flip,
        zoom_range=zoom_range,
        fill_mode='constant',
        cval=0,
    )
    y_aug = ImageDataGenerator(
        rotation_range=rot_range,
        width_shift_range=w_shift_range,
        height_shift_range=h_shift_range,
        horizontal_flip=h_flip,
        vertical_flip=v_flip,
        zoom_range=zoom_range,
        fill_mode='constant',
        cval=0,
    )

    train_gen = data.gen_batches(
        *train,
        batch_size=batch_size,
        x_aug=x_aug,
        y_aug=y_aug
    )
    test_gen = ImageDataGenerator()

    # Train and evaluate models
    roc_auc_scores = []
    for i in range(repeats):
        # Construct training callbacks
        checkpoint = ModelCheckpoint(
            filepath=f'../models/{model_factory.__name__}/{mode}_scores_model.h5',
            save_best_only=True)
        logger = CSVLogger(f'../models/{model_factory.__name__}/{pd.Timestamp.now().date()}_{mode}_errors.csv')
        lr_schduler = CyclicLRScheduler(
            lr0=lr0,
            total_steps=epochs * batches_per_epoch,
            cycles=cycles
        )

        model = model_factory(
            activation=activation,
            depth=depth,
            filters=filters,
            final_activation=final_activation,
            input_shape=train.x.shape[1:],
            loss=loss,
            noise_std=noise_std,
        )
        if verbose and not i:
            model.summary(print_fn=logging.info)

        model.fit_generator(
            train_gen,
            steps_per_epoch=batches_per_epoch,
            epochs=epochs,
            validation_data=test_gen.flow(*val, batch_size=batch_size),
            callbacks=[checkpoint, logger, lr_schduler],
            verbose=verbose,
        )
        # Load best checkpoint
        model = load_model(
            f'../models/{model_factory.__name__}/{mode}_scores_model.h5',
            custom_objects={'iou_loss': losses.iou_loss}
        )

        # Evaluate performance on the test set
        test_pred = model.predict_generator(
            test_gen.flow(test.x, batch_size=batch_size, shuffle=False),
            verbose=verbose,
        )

        micro_auc = metrics.roc_auc_score(
            y_true=test.y.flatten(),
            y_score=test_pred.flatten(),
            average='micro',
        )
        macro_auc = metrics.roc_auc_score(
            y_true=test.y.flatten(),
            y_score=test_pred.flatten(),
            average='macro',
        )
        weighted_auc = metrics.roc_auc_score(
            y_true=test.y.flatten(),
            y_score=test_pred.flatten(),
            average='weighted',
        )
        roc_auc_scores.append([micro_auc, macro_auc, weighted_auc])

        logging.info(f'ROC AUC scores {i + 1}: {roc_auc_scores[-1]}')

        # Clean up tensorflow resources before continuing
        K.clear_session()

    scores_df = pd.DataFrame(
        np.stack(roc_auc_scores),
        columns=['micro', 'macro', 'weighted'],
    )
    scores_df.to_csv(
        f'../models/{model_factory.__name__}/{mode}_scores.csv'
    )

    # Calculate score distribution statistics
    col2mean, col2sem, col2interval = dict(), dict(), dict()
    for col in scores_df.columns:
        vals = scores_df[col]
        mean = np.mean(vals)
        sem = stats.sem(vals)
        interval = stats.t.interval(
            percentile,
            len(vals) - 1,
            loc=mean,
            scale=sem,
        )
        col2mean[col] = mean
        col2sem[col] = sem
        col2interval[col] = interval
    logging.info(f'Score means: {col2mean}')
    logging.info(f'Score standard errors: {col2sem}')
    logging.info(f'Score mean {percentile * 100:0.2f}% confidence intervals: {col2interval}')
    return col2interval['macro']


if __name__ == '__main__':
    args = vars(arg_parser().parse_args())

    Path(f'../models/{args["model_factory"].__name__}').mkdir(parents=True, exist_ok=True)
    logging.basicConfig(
        filename=f"../models/{args['model_factory'].__name__}/{pd.Timestamp.now().date()}_{args['mode']}.log",
        level=logging.INFO
    )
    if args['verbose']:
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.info('Command line call: {}'.format(' '.join(sys.argv)))

    if 'classify' in args['mode']:
        roc_auc_conf_intervals(**args)
    elif 'regress' in args['mode']:
        regression_score_conf_intervals(**args)
    else:
        raise ValueError(f'mode argument should contain classify or regress: Given {args["mode"]}')
