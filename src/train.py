"""
Provides a CLI to train Keras models on simulated 12CO, 13CO, and density data.
"""

import argparse
import logging
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from keras import backend as K
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import Sequence
from vailtools import losses
from vailtools import networks
from vailtools.callbacks import CyclicLRScheduler
from vailtools.data.splits import split_data

import data


def arg_parser():
    parser = argparse.ArgumentParser(
        description='Trains a neural network to identify shells in density or 12CO data.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        '-a',
        '--activation',
        default='selu',
        type=str,
        help='Activation function; used everywhere except the final layer.',
    )
    parser.add_argument(
        '-b',
        '--batch_size',
        default=8,
        type=int,
        help='Number of samples used in each gradient update.',
    )
    parser.add_argument(
        '-c',
        '--cycles',
        default=5,
        type=int,
        help='Number of cycles used in cyclic learning rate annealing strategy during training.',
    )
    parser.add_argument(
        '-d',
        '--depth',
        default=4,
        type=int,
        help='Controls the depth (i.e. number of layers) of the constructed network.',
    )
    parser.add_argument(
        '-e',
        '--epochs',
        default=200,
        type=int,
        help='Number of times the entire training set is exposed to the network during training.',
    )
    parser.add_argument(
        '-f',
        '--filters',
        default=16,
        type=int,
        help='Number of filters used for each convolution operation, adjusted by spatial resampling.',
    )
    parser.add_argument(
        '-fa',
        '--final_activation',
        default='sigmoid',
        type=str,
        help='Activation function applied as the last operation of the network.',
    )
    parser.add_argument(
        '-hf',
        '--h_flip',
        default=False,
        type=bool,
        help='Toggles the application of random horizontal flip data augmentation.',
    )
    parser.add_argument(
        '-hs',
        '--h_shift_range',
        default=0.,
        type=float,
        help='Maximum fraction for random vertical shift data augmentation.',
    )
    parser.add_argument(
        '-l',
        '--loss',
        default='iou_loss',
        type=lambda x: getattr(losses, x),
        help='Loss function used to direct the network during training'
    )
    parser.add_argument(
        '-lr0',
        '--lr0',
        default=0.2,
        type=float,
        help='Maximum learning rate value; attained at the start of each learning rate cycle.',
    )
    parser.add_argument(
        '-m',
        '--mode',
        default='classify_co',
        choices=['classify_co', 'regress_co', 'classify_density', 'regress_density'],
        type=lambda x: x.strip().lower(),
        help='Determines what data is used for training and what pre-processing is applied.',
    )
    parser.add_argument(
        '-mf',
        '--model_factory',
        default='res_u_net',
        type=lambda x: getattr(networks, x),
        help='A factory method that constructs a Keras model, see vailtools.networks for available options.',
    )
    parser.add_argument(
        '-n',
        '--noise_std',
        default=0.,
        type=float,
        help='Standard deviation of additive, zero mean Gaussian noise used for data augmentation.',
    )
    parser.add_argument(
        '-r',
        '--rot_range',
        default=0.,
        type=float,
        help='Maximum rotation range (in degrees) for random rotation data augmentation.',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        default=0,
        help='Determines the level of terminal output.',
    )
    parser.add_argument(
        '-vf',
        '--v_flip',
        default=False,
        type=bool,
        help='Toggles the application of random vertical flip data augmentation.',
    )
    parser.add_argument(
        '-ws',
        '--w_shift_range',
        default=0.,
        type=float,
        help='Maximum fraction for random horizontal shift data augmentation.',
    )
    parser.add_argument(
        '-z',
        '--zoom_range',
        default=0.,
        type=float,
        help='Maximum zoom level for random zoom data augmentation.',
    )
    return parser


def train_nn(
        activation='selu',
        batch_size=8,
        cycles=5,
        depth=4,
        epochs=200,
        filters=16,
        final_activation='sigmoid',
        h_flip=False,
        h_shift_range=0.,
        loss='iou_loss',
        lr0=0.2,
        mode='classify_co',
        model_factory=networks.res_u_net,
        noise_std=0.,
        rot_range=0,
        v_flip=False,
        verbose=False,
        w_shift_range=0.,
        zoom_range=0.,
        **kwargs,
):
    """
    Trains a Keras model obtained from model_factory using the provided training
    configuration.

    Utilizes the cyclic learning rate schedule discussed in https://arxiv.org/abs/1704.00109
    
    Args:
        activation: str, keras function, or tensorflow function,
        batch_size: int,
        cycles: int,
        depth: int,
        epochs: int,
        filters: int,
        final_activation: str, keras function, or tensorflow function,
        h_flip: bool,
        h_shift_range: float,
        loss: str, keras function, or tensorflow function,
        lr0: float,
        mode: str,
        model_factory: Callable[[**kwargs], keras.models.Model],
        noise_std: float,
        rot_range: int,
        v_flip: bool,
        verbose: bool,
        w_shift_range: float,
        zoom_range: float or tuple(float, float),

    Returns: float,
        Test error of the trained model.
    """
    # Prepare data and training set
    x, y = getattr(data, f'load_{mode.split("_")[-1]}_data')()
    train, val, test = split_data(
        x,
        y,
        save=True,
        save_path=f'../models/{model_factory.__name__}',
        save_name=f'{mode}_splits',
        process_func=getattr(data, f'process_{mode}_data')
    )

    batches_per_epoch = int(np.ceil(len(train.x) / batch_size))

    # Prepare model and helpers
    model = model_factory(
        activation=activation,
        depth=depth,
        filters=filters,
        final_activation=final_activation,
        input_shape=train.x.shape[1:],
        loss=loss,
        noise_std=noise_std,
    )
    if verbose:
        model.summary(print_fn=logging.info)

    x_aug = ImageDataGenerator(
        rotation_range=rot_range,
        width_shift_range=w_shift_range,
        height_shift_range=h_shift_range,
        horizontal_flip=h_flip,
        vertical_flip=v_flip,
        zoom_range=zoom_range,
        fill_mode='constant',
        cval=0,
    )
    y_aug = ImageDataGenerator(
        rotation_range=rot_range,
        width_shift_range=w_shift_range,
        height_shift_range=h_shift_range,
        horizontal_flip=h_flip,
        vertical_flip=v_flip,
        zoom_range=zoom_range,
        fill_mode='constant',
        cval=0,
    )

    train_gen = data.gen_batches(
        *train,
        batch_size=batch_size,
        x_aug=x_aug,
        y_aug=y_aug
    )
    test_gen = ImageDataGenerator()

    checkpoint = ModelCheckpoint(
        filepath=f'../models/{model_factory.__name__}/{mode}_model.h5',
        save_best_only=True)
    logger = CSVLogger(f'../models/{model_factory.__name__}/{pd.Timestamp.now().date()}_{mode}_errors.csv')
    lr_schduler = CyclicLRScheduler(
        lr0=lr0,
        total_steps=epochs * batches_per_epoch,
        cycles=cycles
    )

    # Train model
    model.fit_generator(
        train_gen,
        steps_per_epoch=batches_per_epoch,
        epochs=epochs,
        validation_data=test_gen.flow(*val, batch_size=batch_size),
        validation_steps=int(np.ceil(len(val.x) / batch_size)),
        callbacks=[checkpoint, logger, lr_schduler],
        verbose=verbose,
    )
    # Load best checkpoint
    model = load_model(
        f'../models/{model_factory.__name__}/{mode}_model.h5',
        custom_objects={'iou_loss': losses.iou_loss}
    )

    # Evaluate performance on the test set
    error = model.evaluate_generator(
        test_gen.flow(*test, batch_size=batch_size, shuffle=False),
        steps=int(np.ceil(len(test.x) / batch_size)),
    )
    logging.info(f'Test error: {error:.6f}')

    # Clean up tensorflow resources before returning
    K.clear_session()
    return error


if __name__ == '__main__':
    args = vars(arg_parser().parse_args())

    Path(f'../models/{args["model_factory"].__name__}').mkdir(parents=True, exist_ok=True)
    logging.basicConfig(
        filename=f"../models/{args['model_factory'].__name__}/{pd.Timestamp.now().date()}_{args['mode']}.log",
        level=logging.INFO
    )
    if args['verbose']:
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.info('Command line call: {}'.format(' '.join(sys.argv)))

    train_nn(**args)
