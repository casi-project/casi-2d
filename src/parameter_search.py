"""
Implements methods that search the hyper-parameter space of models used in the
NSB project in order to improve model configurations.
"""

from pathlib import Path
import shutil

import numpy as np
import pandas as pd
import tensorflow as tf
from keras.backend import clear_session

from train import train_nn


def main():
    hyper_search(iters=50)


def hyper_search(iters, param_log_file='../models/dilated_res_net/search_params.csv'):
    """
    Randomly explores the hyper-parameter space, saves the best model, and
    outputs runtime statistics.
    Logs configurations and their performance to a csv file.

    Args:
        iters (int) Number of parameter sets to test.

        param_log_file (str) Path to the search history log.
    """
    with open('../models/dilated_res_net/search.log', 'w') as f:
        f.write('')

    if Path(param_log_file).is_file():
        param_log = pd.read_csv(param_log_file)
        best_error = np.min(param_log.error)
        best_params = param_log.iloc[np.argmin(param_log.error)]
    else:
        param_log = pd.DataFrame()
        best_error = np.inf
        best_params = {}

    for i in range(iters):
        print(f'Search progress: {i+1}/{iters}\n\tBest Error={best_error}')
        params = {
            'noise_std': np.random.exponential(scale=0.1),
            'rot_range': np.random.randint(low=361),
            'w_shift_range': np.random.exponential(scale=0.1),
            'h_shift_range': np.random.exponential(scale=0.1),
            'h_flip': np.random.choice([True, False]),
            'v_flip': np.random.choice([True, False]),
            'zoom_range': np.random.exponential(scale=0.1)}

        try:
            error = train_nn(**params, verbose=False)
        except KeyboardInterrupt:
            break
        except tf.errors.ResourceExhaustedError:
            with open('../models/dilated_res_net/search.log', 'a') as f:
                f.write(f'Memory Exhausted during iteration {i} at {pd.Timestamp.now()}\n')
            error = np.inf

        params['timestamp'] = pd.Timestamp.now()
        params['error'] = error

        if error < best_error:
            print(f'Minimum error updated: {error}\n')
            best_error = error
            best_params = params
            shutil.copyfile(
                '../models/dilated_res_net/model.h5',
                '../models/dilated_res_net/search_model.h5',
            )
            shutil.copyfile(
                '../models/dilated_res_net/training_log.csv',
                '../models/dilated_res_net/search_errors.csv',
            )

        param_log = pd.concat([param_log, pd.DataFrame([params])],
                              ignore_index=True)
        param_log.to_csv(param_log_file, index=False)

        # Free resources for the next iteration
        clear_session()
    print(f'Hyper-parameter sweep complete. Best parameters\n{best_params}')


if __name__ == '__main__':
    main()
