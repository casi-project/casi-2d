"""
Estimates the background noise present in several volumes of observational CO data
and provides descriptive statistics. This may be used to inform data augmentation
techniques when training neural networks.
"""

import numpy as np
from scipy.signal import convolve2d

import data


def main():
    for name in ['ngcc1333', 'perseus']:
        bg_noise_estimate_summary(getattr(data, f'get_{name}_data'), estimate_bg_noise_conv2d)


def bg_noise_estimate_summary(get_data, estimate_noise):
    """
    Estimates the background noise over a stack of images and provides descriptive
    statistics for the resulting distribution.

    Args:
        get_data: Callable, Returns a 3D numpy array of data when called.
        estimate_noise: Callable, Takes a 2D image and returns an estimate of the
            background noise.
    """
    print(f'{get_data.__name__[4:-5].title()}:')
    sigmas = np.asarray([estimate_noise(x) for x in np.squeeze(get_data())])
    print(
        f'\tMax:  {sigmas.max():0.6f}'
        f'\n\t75%:  {np.percentile(sigmas, 75):0.6f}'
        f'\n\t50%:  {np.percentile(sigmas, 50):0.6f}'
        f'\n\t25%:  {np.percentile(sigmas, 25):0.6f}'
        f'\n\tMin:  {sigmas.min():0.6f}'
        f'\n\tMean: {sigmas.mean():0.6f}'
        f'\n\tStd:  {sigmas.std():0.6f}'
    )


def estimate_bg_noise_conv2d(image):
    """
    Estimates the background noise in an image by convolving the input image with
    a 2D kernel.

    Credit to:
        https://stackoverflow.com/questions/2440504/noise-estimation-noise-measurement-in-image
    """
    height, width = image.shape
    filter_ = [
        [1, -2, 1],
        [-2, 4, -2],
        [1, -2, 1]
    ]
    sigma = np.sum(np.sum(np.absolute(convolve2d(image, filter_))))
    sigma = sigma * np.sqrt(0.5 * np.pi) / (6 * (width - 2) * (height - 2))
    return sigma


if __name__ == '__main__':
    main()
