import argparse
from pathlib import Path

import numpy as np
from astropy.io import fits
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from vailtools import losses

from data import get_co_files, load_fits, prediction_to_fits


def main(model_path, verbose=False):
    analyze_ngcc1333(model_path, verbose=verbose)
    analyze_perseus_b5(model_path, verbose=verbose)


def analyze_ngcc1333(
        model_path,
        observation_path='../data/observational/ngc1333_13co.fits',
        verbose=False):
    with fits.open(observation_path) as f:
        x = f[0].data
    x = np.transpose(x)
    x = np.where(np.isnan(x), np.random.normal(0, 0.15, size=x.shape), x)
    x = np.pad(x,
               ((0, 0), (0, 175), (0, 160), (0, 0)),
               'constant',
               constant_values=0.)
    x -= x.mean()
    x /= x.std()

    model = load_model(model_path,
                       custom_objects={'iou_loss': losses.iou_loss})
    gen = ImageDataGenerator(fill_mode='constant', cval=0)
    prediction = model.predict_generator(gen.flow(x, shuffle=False), verbose=verbose)
    prediction_to_fits(
        prediction,
        out_path=f'{Path(model_path).parent}/{Path(model_path).stem[:-6]}_{Path(observation_path).stem}.fits'
    )


def analyze_perseus_b5(
        model_path,
        observation_path='../data/observational/perseus_b5_12co_nnorm.fits',
        verbose=False):
    with fits.open(observation_path) as f:
        x = f[0].data
    x = np.where(np.isnan(x), np.random.normal(0, 0.15, size=x.shape), x)
    x = np.expand_dims(x, axis=-1)
    x = np.pad(x,
               ((0, 0), (0, 76), (0, 76), (0, 0)),
               'constant',
               constant_values=0.)
    x -= x.mean()
    x /= x.std()

    model = load_model(model_path,
                       custom_objects={'iou_loss': iou_loss})
    gen = ImageDataGenerator(fill_mode='constant', cval=0)
    prediction = model.predict_generator(gen.flow(x, shuffle=False), verbose=verbose)
    prediction_to_fits(prediction, ref_file_path=observation_path, out_path=f'{Path(model_path).parent}/'
                                                                            f'{Path(model_path).stem[:-6]}_{Path(observation_path).stem}.fits')


def get_train_norm_consts(data_path):
    x = np.concatenate(load_fits(get_co_files(data_path)))

    return np.min(x), np.mean(x), np.std(x)


def verify_file(path):
    if Path(path).is_file():
        return path
    else:
        raise ValueError('The provided path, "{}", is not a file. '
                         'Please provide a valid path to a trained keras model.')


def arg_parser():
    parser = argparse.ArgumentParser(description='Use a pre-trained ANN to identify bubbles in astronomical images.')
    parser.add_argument('model_path', type=verify_file)
    parser.add_argument('-v', '--verbose', action='count')
    return parser


if __name__ == '__main__':
    main(**vars(arg_parser().parse_args()))
