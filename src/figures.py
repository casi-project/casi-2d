"""
Visualizations to display network performance in several situations.
"""

import argparse
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from keras.models import model_from_json
from keras.utils import plot_model
from matplotlib import cbook
from matplotlib import colors
from matplotlib.colors import Normalize
from numpy import ma
from pathlib import Path
from sklearn import metrics


def main():
    parser = argparse.ArgumentParser(
        description='Create visualizations from files generated during training or evaluation of NSB models.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        'file_path',
        type=argparse.FileType('r'),
        help='A file used in visualization generation.',
    )
    parser.add_argument(
        '-m',
        '--mode',
        type=str,
        default='error_curves',
        help='Determines what visualization is created.',
    )
    args = parser.parse_args()

    if args.mode == 'error_curves':
        error_curves(args.file_path)


def diff_overlays(x, y_true, y_pred, threshold=0.5, output_path='.', norm=None):
    """
    Creates a set of layered figures that display the prediction performance of
    binary segmentation algorithms.
    x, y_true, and y_pred are all assumed to have the same shape.

    Args:
        x: numpy.ndarray, input data
        y_true: numpy.ndarray, binary labels
        y_pred: numpy.ndarray, binary predictions
        threshold: float, Cutoff used to binarize the prediction
        output_path: str, path to desired output folder/directory
        norm: matplotlib colormap normalizer, used to rescale the colormap.
    """
    Path(output_path).mkdir(parents=True, exist_ok=True)

    for i, ims in enumerate(zip(x, y_true, y_pred)):
        if norm == 'symlog':
            norm = colors.SymLogNorm(0.001, vmin=ims[0].min(), vmax=ims[0].max())
        plt.imshow(
            np.squeeze(ims[0]),
            cmap=plt.get_cmap('Greys'),
            origin='lower',
            norm=norm,
        )

        true = np.squeeze(ims[1])
        pred = np.where(np.squeeze(ims[2]) > threshold, 1., 0.)
        diff = true - pred
        diff = np.ma.masked_where((true + pred) < 0.5, diff)
        if np.sum(diff):
            plt.imshow(
                diff,
                cmap=plt.get_cmap('viridis'),
                origin='lower',
                alpha=0.5,
                vmin=-1.,
                vmax=2.,
            )
        plt.tick_params(
            axis='both',
            which='both',
            top=False,
            bottom=False,
            right=False,
            left=False,
            labelleft=False,
            labelbottom=False,
        )
        plt.title(f'Test slice {i}')
        plt.savefig(f'{output_path}/diff_{i}.png')
        plt.close()


def residual_overlays(x, residuals, output_path='.', x_norm=None, r_norm='midpoint'):
    """
    Creates a set of layered figures that display the prediction performance of
    dense regression algorithms.
    x, y_true, and y_pred are all assumed to have the same shape.

    Args:
        x: numpy.ndarray, input data
        residuals: numpy.ndarray, Residuals of the predictions
        output_path: str, path to desired output folder/directory
        x_norm: matplotlib colormap normalizer, used to rescale the x colormap.
        r_norm: matplotlib colormap normalizer, used to rescale the residual colormap.
    """
    Path(output_path).mkdir(parents=True, exist_ok=True)
    x = np.squeeze(x)
    residuals = np.squeeze(residuals)

    if x_norm == 'symlog':
        x_norm = colors.SymLogNorm(1e-5, vmin=x.min(), vmax=x.max())

    if r_norm == 'midpoint':
        r_norm = MidPointNorm(midpoint=0, vmin=x.min(), vmax=x.max())
    elif r_norm == 'symlog':
        r_norm = colors.SymLogNorm(1e-5, vmin=residuals.min(), vmax=residuals.max())

    for i, (x_, diff) in enumerate(zip(x, residuals)):
        plt.imshow(
            x_,
            cmap=plt.get_cmap('Greys'),
            origin='lower',
            norm=x_norm,
        )

        if np.sum(np.abs(diff)):
            plt.imshow(
                diff,
                cmap=plt.get_cmap('bwr'),
                origin='lower',
                alpha=0.5,
                vmin=residuals.min(),
                vmax=residuals.max(),
                norm=r_norm,
            )
            plt.colorbar()
        plt.tick_params(
            axis='both',
            which='both',
            top=False,
            bottom=False,
            right=False,
            left=False,
            labelleft=False,
            labelbottom=False,
        )
        plt.title(f'Test slice {i}')
        plt.savefig(f'{output_path}/residual_{i}.png')
        plt.close()


def roc_curve(y_true, y_scores, output_path='.'):
    """
    Derived from sklearn example:
        http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html

    Args:
        y_true:
        y_scores:
        output_path:
    """
    fpr, tpr, thresholds = metrics.roc_curve(y_true.flatten(), y_scores.flatten())
    roc_auc = metrics.auc(fpr, tpr)
    plt.figure()
    plt.plot(fpr, tpr, color='darkorange', lw=2, label=f'ROC curve (AUC = {roc_auc:0.4f})')
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([-0.01, 1.0])
    plt.ylim([0.0, 1.01])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.legend(loc="lower right")
    plt.savefig(f'{output_path}/roc_curve.png')
    plt.close()

    return fpr, tpr, thresholds, roc_auc


def residual_hist(residuals, title=None, xlabel=None, ylabel=None, output_path='.'):
    plt.figure()
    bins = np.histogram_bin_edges(residuals.flatten(), bins='auto')
    plt.hist(residuals.flatten(), bins=bins, density=True, histtype='stepfilled')
    if title is not None:
        plt.title(title)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    plt.savefig(f'{output_path}/residual_histogram.png')
    plt.close()


def residual_scaling(predictions, residuals, title=None, xlabel=None, ylabel=None, output_path='.'):
    plt.figure()

    plt.plot(
        [0., 0.],
        [predictions.min(), predictions.max()],
        lw=2,
        color='k',
        zorder=1,
    )

    plt.scatter(
        residuals.flatten(),
        predictions.flatten(),
        alpha=0.75,
        marker='.',
        zorder=2,
    )
    if title is not None:
        plt.title(title)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    plt.savefig(f'{output_path}/residual_scaling.png')
    plt.close()


def residual_scaling_hist(values, residuals, bins=200, title=None, xlabel=None, ylabel=None, output_path='.'):
    plt.figure()

    plt.hist2d(
        residuals.flatten(),
        values.flatten(),
        bins=bins,
        normed=True,
        norm=colors.LogNorm(),
        cmap=plt.get_cmap('viridis'),
    )
    plt.colorbar()
    if title is not None:
        plt.title(title)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if ylabel is not None:
        plt.ylabel(ylabel)
    plt.savefig(f'{output_path}/residual_scaling_hist.png')
    plt.close()


def error_curves(file_path, y_label='Loss', out_path=None):
    df = pd.read_csv(file_path)
    df.columns = ['Epoch', 'Training', 'Validation']
    df.set_index('Epoch').plot(alpha=0.8)

    plt.xlabel('Epoch')
    plt.ylabel(y_label)
    plt.title('Model Convergence')
    plt.grid()

    p = Path(file_path)
    out_path = out_path or (p.parent / 'figs' / f'{p.stem}.png')
    plt.savefig(out_path)
    plt.close()


def architecture(name, out_path='../visualizations'):
    with open(f'../ModelConfigs/{name}.json', 'r') as f:
        model = model_from_json(f.read())

    plot_model(model,
               to_file=f'{out_path}/{name}_TB.png',
               show_shapes=True,
               rankdir='TB')
    plot_model(model,
               to_file=f'{out_path}/{name}_LR.png',
               show_shapes=True,
               rankdir='LR')


def spatial_error(model_name):
    data_path = f'../models/{model_name}/outputs.npz'

    with np.load(data_path) as data:
        y_true, y_pred = data['Y'], data['P']

    spatial_mae = np.mean(np.abs(y_true - y_pred), axis=0)

    fig, ax = plt.subplots()

    ax.set_title('Spatial MAE')
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])

    cax = ax.imshow(np.squeeze(spatial_mae), cmap='Greys')
    fig.colorbar(cax)

    fig.savefig(f'../models/{model_name}/figs/mae_dist.png')

    spatial_mse = np.mean((y_true - y_pred) ** 2, axis=0)

    fig, ax = plt.subplots()

    ax.set_title('Spatial MSE')
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])

    cax = ax.imshow(np.squeeze(spatial_mse), cmap='Greys')
    fig.colorbar(cax)

    fig.savefig(f'../models/{model_name}/figs/mse_dist.png')


def nsf_proposal_5(model_name, inds=None, data_path=None):
    if data_path is None:
        data_path = f'../models/{model_name}/outputs.npz'
    if inds is None:
        # co_indicies
        # inds = [667, 108, 883]
        # density indicies
        inds = [200, 400, 600]

    with np.load(data_path) as data:
        x, y_true, y_pred = data['X'], data['Y'], data['P']

    x, y_true, y_pred = x[inds], y_true[inds], y_pred[inds]
    arrays = [np.squeeze(arr) for arr in (x, y_true, y_pred)]

    f, axes = plt.subplots(len(inds), 3, figsize=(7, 7))
    vmin = np.min(np.c_[x, y_true, y_pred])
    vmax = np.max(np.c_[x, y_true, y_pred]) / 4
    for i in range(len(inds)):
        for j in range(3):
            axes[i, j].imshow(arrays[j][i], vmin=vmin, vmax=vmax, cmap='Greys')
            axes[i, j].get_xaxis().set_ticks([])
            axes[i, j].get_yaxis().set_ticks([])

    axes[0, 0].set_title('$^{12}$CO Data')
    axes[0, 0].title.set_fontsize(20)
    axes[0, 1].set_title('Tracer')
    axes[0, 1].title.set_fontsize(20)
    axes[0, 2].set_title('Prediction')
    axes[0, 2].title.set_fontsize(20)

    axes[0, 0].set_ylabel('1', rotation=0, labelpad=20)
    axes[0, 0].yaxis.label.set_fontsize(20)
    axes[1, 0].set_ylabel('2', rotation=0, labelpad=20)
    axes[1, 0].yaxis.label.set_fontsize(20)
    axes[2, 0].set_ylabel('3', rotation=0, labelpad=20)
    axes[2, 0].yaxis.label.set_fontsize(20)

    plt.savefig(f"../models/{model_name}/nsf_prop_fig_5.png")
    plt.close()


class MidPointNorm(Normalize):
    """
    Borrowed from:
        https://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    """
    def __init__(self, midpoint=0, vmin=None, vmax=None, clip=False):
        Normalize.__init__(self, vmin, vmax, clip)
        self.midpoint = midpoint

    def __call__(self, value, clip=None):
        if clip is None:
            clip = self.clip

        result, is_scalar = self.process_value(value)

        self.autoscale_None(result)
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if not (vmin < midpoint < vmax):
            raise ValueError("midpoint must be between maxvalue and minvalue.")
        elif vmin == vmax:
            result.fill(0)  # Or should it be all masked? Or 0.5?
        elif vmin > vmax:
            raise ValueError("maxvalue must be bigger than minvalue")
        else:
            vmin = float(vmin)
            vmax = float(vmax)
            if clip:
                mask = ma.getmask(result)
                result = ma.array(np.clip(result.filled(vmax), vmin, vmax),
                                  mask=mask)

            # ma division is very slow; we can take a shortcut
            resdat = result.data

            # First scale to [-1, 1], then to [0, 1].
            resdat -= midpoint
            resdat[resdat > 0] /= np.abs(vmax - midpoint)
            resdat[resdat < 0] /= np.abs(vmin - midpoint)

            resdat /= 2.
            resdat += 0.5
            result = ma.array(resdat, mask=result.mask, copy=False)

        if is_scalar:
            result = result[0]
        return result

    def inverse(self, value):
        if not self.scaled():
            raise ValueError("Not invertible until scaled")
        vmin, vmax, midpoint = self.vmin, self.vmax, self.midpoint

        if cbook.iterable(value):
            val = ma.asarray(value)
            val = 2 * (val - 0.5)
            val[val > 0] *= np.abs(vmax - midpoint)
            val[val < 0] *= np.abs(vmin - midpoint)
            val += midpoint
            return val
        else:
            val = 2 * (value - 0.5)
            if val < 0:
                return val * np.abs(vmin-midpoint) + midpoint
            else:
                return val * np.abs(vmax-midpoint) + midpoint


if __name__ == '__main__':
    main()
