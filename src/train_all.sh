#!/usr/bin/env bash

python train.py -m classify_density -e 200 -n 0.003 -v
python evaluate.py -m classify_density -e 200 -n 0.003 -v

python train.py -m regress_density -fa linear -l mse -e 200 -n 0.003 -v
python evaluate.py -m regress_density -fa linear -l mse -e 200 -n 0.003 -v

python train.py -m classify_co -e 200 -v
python evaluate.py -m classify_co -e 200 -v

python train.py -m regress_co -fa linear -l mse -e 200 -n 0.003 -v
python evaluate.py -m regress_co -fa linear -l mse -e 200 -n 0.003 -v
