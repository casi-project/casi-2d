#!/usr/bin/env python
# -*- coding: utf-8 -*-

import io
import os

import setuptools


# Package meta-data.
NAME = 'CASI-2D'
DESCRIPTION = 'Identifies shells in density, 12CO, and 13CO data.'
URL = 'https://gitlab.com/casi-project/casi-2d'
EMAIL = 'cvanoort156@gmail.com'
AUTHOR = 'Colin Van Oort'
REQUIRES_PYTHON = '>=3.6.0'
VERSION = 1.0

# Required packages
REQUIRED = [
    'astropy',
    'numpy',
    'pandas',
    'scikit-learn',
    'scipy'
    'keras',
    'tensorflow',
    'vailtools',
]

# Optional packages
EXTRAS = {
    'matplotlib',
}

# Path to package top-level
here = os.path.abspath(os.path.dirname(__file__))

# Import the README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
try:
    with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    with open(os.path.join(here, NAME, '__version__.py')) as f:
        exec(f.read(), about)
else:
    about['__version__'] = VERSION

setuptools.setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=setuptools.find_packages(exclude=('tests',)),
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license='MIT',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
)
