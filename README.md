# Convolutional Approach to Shell Identification - 2D (`CASI-2D`)
## [Documentation](https://casi-project.gitlab.io/casi-2d)
## [Our Paper](https://iopscience.iop.org/article/10.3847/1538-4357/ab275e/meta)
## Description
The goal of this project is to produce an automated method for identifying shells, sometimes referred to as "bubbles", and other structures of interest in molecular cloud data.

Currently, the most common process for identifying such shells is manual identification by a panel of domain experts, which is slow, resource intensive, and may introduce subjective biases.

Some efforts have been made to automate this process using 
[support vector machines](https://arxiv.org/abs/1107.5584),
[random forests](https://arxiv.org/abs/1406.2692), and artificial neural networks
(ANN) [[1](http://iopscience.iop.org/article/10.1086/375571/meta), 
[2](http://iopscience.iop.org/article/10.1086/513501/meta)], though these efforts
have not yet resulted in the development of tools capable of automatically processing
survey data.

We are investigating the application of modern ANN architectures, such as the
[U-Net](https://arxiv.org/abs/1505.04597), which have yet to be widely applied in
the field of Astronomy.

## Contents
 * data: .fits files containing density, CO, and tracer data used to train and test our models.
 * docs: Automatically generated documentation created using [Doxygen](http://www.doxygen.nl/).
 * models: Pre-trained models and associated output files, including figures and tables.
 * src: Source code for building, training, and evaluating shell identifiers.
    * confusion.py: Classifier evaluation via confusion matrix statistics.
    * data.py: Data I/O and pre-processing functions.
    * evaluate.py: Trained network evaluation.
    * figures.py: Figure generation using [matplotlib](https://matplotlib.org/), facilitates evaluation of trained models.
    * noise_estimation.py: Estimates the global background noise intensity in observational data.
    * parameter_search.py: Uses random search to optimize network hyper-parameters.
    * real_data_test.py: Applies trained networks to observational data.
    * score_confidence_intervals.py: Estimates confidence intervals for select measures of model performance.
    * train.py: Trains new networks from scratch.
    * train_all.sh: A shell file that uses train.py and evaluate.py to create several models.
    * training_variance.py: Evaluates the consistency of the training procedure over several replicates.
 * install.sh: A convenience script to simplify project setup and installation.
 * LICENSE.txt: Information about distribution and usage.
 * README.md: High level overview of the organization and current state of the project.
 * requirements.txt: Python dependencies required for full functionality.

## Getting Started
### Required Tools
This project is hosted on GitLab, thus the primary tool for interacting with the
source code is `Git`.
Additionally, we utilized `Git-LFS` (Large File Storage) in order to provide training
data along with the source code.
The primary language of the project is `Python 3.6`, but there are some convenience
scripts for assisting in project setup and experiment execution that use `Bash`.
Finally, the installation script requires `curl`, a data transfer tool that is
used to download some dependencies.

Users should ensure that `Git`, `Git-LFS`, `Bash`, and `curl` are properly installed
before attempting to work with `CASI`.

**Note:** If git-lfs was not installed prior to cloning the repository, then users
may need to run
```bash
cd casi-2d
git lfs install
git lfs pull
```
in order to properly obtain the data files, since they start as pointers following
a clone where git-lfs is absent.

Additional information about `Git-LFS` can be found [here](https://git-lfs.github.com/),
and an introductory guide is available [here](https://www.atlassian.com/git/tutorials/git-lfs#installing-git-lfs).


### Python Environment
We recommend using [Anaconda](https://www.anaconda.com/) to manage the Python
dependencies required by this project.
In particular, we recommend the [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
variant, which provides the same tools for managing python packages, but downloads
less packages in the initial installation.
Command-line and GUI installers for Anaconda are available
[here](https://www.anaconda.com/distribution/) and Miniconda installers are
available [here](https://docs.conda.io/en/latest/miniconda.html).
 
Additionally, we recommend creating a conda environment in order to reduce the 
chances of dependency version conflicts with other packages.

### Installing CASI
`install.sh` provides a convenient method for the installation of `CASI` on
Linux/Unix machines that conforms to the recommendations above.

If `install.sh` fails, or the user wishes to adjust aspects of the installation,
then `install.sh` may serve as a useful guide, providing example usages of the
commands that users will need in order to manually install `CASI`.

Users who experience difficulty with the installation script, or those who wish
to modify aspects of the installation, will need to follow these steps:

- Clone the repository
- Install the Python packages listed in `requirements.txt`
  + Note that `VaiL Tools` is also a GitLab repository and should be installed
  following the [instructions](https://gitlab.com/vail-uvm/vailtools#installation)
  provided by the repository.
- Ensure that `Git-LFS` is properly initialized in the repository and that the
data has been de-referenced.

### A Note on GPU Acceleration
The methods implemented in the `CASI` project rely on Graphics Processing Unit
(GPU) acceleration in order to reduce training times.
Support for different flavors of GPU hardware (Nvidia, AMD, etc.) is determined by
the deep learning libraries used to build `CASI`, namely `Keras` and `Tensorflow`.
In general, Nvidia hardware is better supported by these libraries, and newer hardware
is better suited to training deep learning models.
Thus, users with AMD hardware or older Nvidia GPUs may experience difficulties
setting up and using `CASI`.
Finally, in order to properly utilize GPU acceleration, please ensure that your
GPU drivers are up to date before installing the `Keras` and `Tensorflow` dependencies.

### Using the `CASI` Tools
New models may be trained using  `train.py`.
The following example trains a Residual U-Network on a CO segmentation task for
200 epochs and provides verbose outputs during training:
```bash
python train.py -mf res_u_net -m classify_co  -e 200 -v
```
Trained models may be evaluated using ```evaluate.py```.
The following example will evaluate the model that was trained in the previous example:
```bash
python evaluate.py -mf res_u_net -m classify_co -v
```

`train.py` and `evaluate.py` feature a comprehensive command line interface (CLI),
implemented using [argparse](https://docs.python.org/3/library/argparse.html),
use the `-h` or `--help` flags to get more information about exposed
functionality.

**Note:** Python source files are expected to be executed in the `src/` directory,
i.e. you should `cd casi-2d/src` before attempting to execute project source files.

## Applying CASI to new data
In order to apply `CASI` to your own data you will need to select a label for
your dataset and implement two functions.
`load_{label}` will load the data from disc and `process_{label}` will prepare
your data so that it may be passed to machine learning models.
Finally, you will need to modify the `mode` CLI argument, defined near the top of
`train.py`.
Note that the load function should not require any arguments, but may have any number
of optional arguments.
The process function should accept two positional arguments, `x` and `y`, the input
and expected output data respectively, along with any number of optional arguments.
Example load and process functions are present in the `data.py` file, which is also
where your implementation should be placed.

Following this, the remainder of the `CASI` CLI tools should function as intended
for your new application!

## Citing CASI
If you plan to use `CASI` or portions of the `CASI` source code in your work then
please cite [our paper](https://iopscience.iop.org/article/10.3847/1538-4357/ab275e/meta).

BibTex:
```
@article{Van_Oort_2019,
	doi = {10.3847/1538-4357/ab275e},
	url = {https://doi.org/10.3847%2F1538-4357%2Fab275e},
	year = 2019,
	month = {jul},
	publisher = {American Astronomical Society},
	volume = {880},
	number = {2},
	pages = {83},
	author = {Colin M. Van Oort and Duo Xu and Stella S. R. Offner and Robert A. Gutermuth},
	title = {{CASI}: A Convolutional Neural Network Approach for Shell Identification},
	journal = {The Astrophysical Journal},
}
```

## Trouble Shooting
This project uses "channels\_last" image data format (i.e. tensors in our models
have the dimensions (images, rows, cols, channels)).
If you encounter a dimension mismatch error when attempting to execute project
source files, check that the "image\_data\_format" entry of your
`$HOME/.keras/keras.json` file is set to "channels\_last".
See the [keras docs](https://keras.io/backend/) for more details.
