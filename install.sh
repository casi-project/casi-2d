#!/usr/bin/env bash


# Download and install Miniconda/Anaconda, if it is not already present
if alias conda 2>/dev/null; then
    echo "Anaconda not detected, beginning installation."

    if [[ "$OSTYPE" == "linux-gnu" ]]; then
        # Linux variants
        curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
        bash Miniconda3-latest-Linux-x86_64.sh -bf
        rm Miniconda3-latest-Linux-x86_64.sh

    else
        echo "$OSTYPE is not a supported operating system."
    fi

    echo "Exposing bash aliases."
    if [[ -f ~/.zshrc ]]; then
        echo 'export PATH=$HOME/miniconda3/bin:$PATH' >> ~/.zshrc
        source ~/.zshrc
    else
        echo 'export PATH=$HOME/miniconda3/bin:$PATH' >> ~/.bashrc
        source ~/.bashrc
    fi
else
    echo "Anaconda detected, skipping installation."
fi


# Create a new conda environment to manage Python dependencies,
# or ensure the existing one has the correct dependencies
if [[ $"conda env list | grep casi" == "casi"* ]]; then
    echo "Conda environment 'casi' detected, updating dependencies."
    source activate casi
    conda install -y keras-gpu astropy pandas matplotlib scikit-learn
else
    echo "Conde environment 'casi' not detected, creating."
    conda create -y -n casi python=3.6 keras-gpu astropy pandas matplotlib scikit-learn
fi


# Install or update the VailTools dependency
if [[ -d ../vailtools ]]; then
    echo "VailTools dependency detected, updating."
    cd ../vailtools

    # Update VailTools using git
    git pull

    # Finalize the updated using the provided convenience script
    source activate casi
    bash install.sh

    cd ../casi-2d

else
    echo "VailTools dependency not detected, installing."
    git clone "https://gitlab.com/vail-uvm/vailtools.git" ../vailtools
    cd ../vailtools

    # Install VailTools using the provided convenience script
    source activate casi
    bash install.sh

    cd ../casi-2d
fi


# Ensure that git-lfs is initialized in the CASI repo
git lfs install

# Ensure that LFS pointers are inflated
git lfs pull
